FROM registry.access.redhat.com/rhel7
MAINTAINER Pietrangelo Masala <p.masala@entando.com>
LABEL name="Entando 4.3.1" \
      maintainer="Pietrangelo Masala <p.masala@entando.com>" \
      vendor="Entando, Inc." \
      version="4.3.1" \
	  release="1.1" \
	  license="lgplv2.1" \
	  summary="Web Application Example based on the Entando 4.3.1 release" \
      description="sample of an Entando web application based on engine, admin-console, portal-ui core components. It is useful to develop and test vertical applications extending engine, admin console and portal-ui features" \
      run="docker run --name=[your-container-name] -it -p 8080:8080 entando/entando-431 " \
      stop="docker stop [your-container-name]" \
      io.k8s.description="Sample of an Entando web application based on engine, admin-console, portal-ui core components." \
      io.k8s.display-name="Entando 4.3.1" \
	  io.openshift.expose-services="8080:http" \
	  io.openshift.tags="entando,modern-ux"


ENV MAVEN_VERSION 3.3.9
ENV JAVA_HOME /opt/jdk1.8.0_144
ENV JRE_HOME /opt/jdk1.8.0_144/jre
ENV MAVEN_HOME /usr/share/maven

USER root
RUN REPOLIST=rhel-7-server-rpms,rhel-7-server-optional-rpms \
    yum -y update-minimal --disablerepo "*" --enablerepo rhel-7-server-rpms --setopt=tsflags=nodocs \
    --security --sec-severity=Important --sec-severity=Critical && \
    yum -y clean all && rm -rf /var/cache/yum 
    #&& groupadd -r 1001 && useradd --no-log-init -r -g 1001 1001

COPY jdk-8u144-linux-x64.tar.gz /opt/

RUN mkdir -p /home/entando/sample/entando-openshift && mkdir -p /licenses
COPY entando-openshift/ /home/entando/sample/entando-openshift
COPY licenses/ /licenses
COPY help.1 /help.1
RUN cd /opt \
&& tar xzf jdk-8u144-linux-x64.tar.gz \
&& rm -f jdk-8u144-linux-x64.tar.gz \
&& cd jdk1.8.0_144/ \
&& ln -s /opt/jdk1.8.0_144/bin/java /usr/bin/java \
&& ln -s /opt/jdk1.8.0_144/bin/jar /usr/bin/jar \
&& ln -s /opt/jdk1.8.0_144/bin/javac /usr/bin/javac \
&& cd /tmp \
&& mkdir -p /usr/share/maven \
&& curl -fsSL http://apache.osuosl.org/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz \
| tar -xzC /usr/share/maven --strip-components=1 \
&& ln -s /usr/share/maven/bin/mvn /usr/bin/mvn \
&& chown -R 1001:0 /home/entando && chmod -R ug+wr /home/entando

USER 1001

CMD ["mvn", "-f", "/home/entando/sample/entando-openshift/", "clean", "-DskipTests", "jetty:run"]

EXPOSE 8080
